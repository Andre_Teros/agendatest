Agenda Test
===========

# Установка
1. Перейдите в папку с проектом и запустите докер контейнер
```bash
docker-compose up -d
```

2. Зайдите в контейнер
```bash
docker exec -ti --user www-data:1000 agendatest_php_1 bash
```
3. Внутри контейнера перейдите в рабочую папку и установите зависимости с композера
```bash
cd web/
composer install
```

4. Запустите воркеры
```bash
bin/console rabbitmq:consumer register & bin/console rabbitmq:consumer track &
```

# Использование 
1. Для регистрации пользователя отправьте POST запрос на адрес http://localhost:8888/register
со следующими параметрами: 'first_name', last_name', 'nick_name' (значение может быть от 3 до 30 символов,
кириллицей или латиницей), 'password' (значение может быть от 6 до 30 символов,
кириллицей или латиницей, так же можно использовать цифры, дефис и нижнее подчеркивание), 'age'
(от 1 до 3 символов, только цифры). В случае успеха, структура ответа будет следующей - {"status":"ok"}.

2. Для авторизации пользователя отправьте POST запрос на адрес http://localhost:8888/auth
с параметрами 'nick_name' и 'password' (правила, как в п.1). В случае успеха, структура ответа будет следующей - 
{"status":"ok","user_id":userId}

3. Для трекинга действий отправьте POST запрос на адрес http://localhost:8888/track со следующими параметрами:
'id', 'id_user', 'source_label', 'date_created'. Параметр 'id_user' - необязателен, если он не указан,
система присвоит его.

4. В случае возникновении ошибки ответ будет следующим {"status":"error","message":errorMessage} или 
{"status":"validation error","errors":[{"path":fieldName,"message":errorMessage},...]}

5. Пример запроса curl -XPOST 'http://localhost:8888/auth' -F "nick_name=nick" -F "password=pass123"
