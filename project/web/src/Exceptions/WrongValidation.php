<?php
declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class WrongValidation extends \Exception
{
    private $errorList;

    public function __construct(ConstraintViolationListInterface $errorList)
    {
        $this->errorList = $errorList;
        parent::__construct('wrong validation');
    }

    public function getErrorList(): ConstraintViolationListInterface
    {
        return $this->errorList;
    }
}
