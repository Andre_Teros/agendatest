<?php
declare(strict_types=1);

namespace App\Controller;

use App\Dto\UserActionDto;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class MainController
{
    private $producer;
    private $serializer;

    public function __construct(ProducerInterface $trackProducer, SerializerInterface $serializer)
    {
        $this->producer = $trackProducer;
        $this->serializer = $serializer;
    }

    public function track(UserActionDto $dto): JsonResponse
    {
        $dto->idUser = $dto->idUser ?? Uuid::uuid4();

        $jsonData = $this->serializer->serialize($dto, 'json');

        $this->producer->setContentType('application/json');
        $this->producer->publish($jsonData);

        return new JsonResponse(['status' => 'ok', 'user_id' => $dto->idUser]);
    }
}
