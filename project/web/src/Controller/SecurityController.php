<?php
declare(strict_types=1);

namespace App\Controller;

use App\Dto\UserAuthDto;
use App\Dto\UserRegisterDto;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Ramsey\Uuid\Uuid;
use SocialTech\StorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

class SecurityController
{
    private $serializer;
    private $producer;
    private $storage;
    private $decoder;
    private $storageDir;

    public function __construct(
        SerializerInterface $serializer,
        ProducerInterface $registerProducer,
        StorageInterface $storage,
        DecoderInterface $decoder,
        string $storageDir
    ) {
        $this->serializer = $serializer;
        $this->producer = $registerProducer;
        $this->storage = $storage;
        $this->decoder = $decoder;
        $this->storageDir = $storageDir;
    }

    public function register(UserRegisterDto $dto): JsonResponse
    {
        if ($this->isUserExists($dto->nickName)) {
            throw new \RuntimeException("User with nick name '{$dto->nickName}' already exists");
        }

        $dto->id = Uuid::uuid4();
        $dto->password = password_hash($dto->password, PASSWORD_BCRYPT);

        $jsonData = $this->serializer->serialize($dto, 'json');

        $this->producer->setContentType('application/json');
        $this->producer->publish($jsonData);

        return new JsonResponse(['status' => 'ok']);
    }

    public function auth(UserAuthDto $dto): JsonResponse
    {
        if (!$this->isUserExists($dto->nickName)) {
            throw new \RuntimeException("User with nick name '{$dto->nickName}' does not exist");
        }

        $userJsonData = $this->storage->load("{$this->storageDir}/{$dto->nickName}.json");
        $userData = $this->decoder->decode($userJsonData, 'json');

        if (!password_verify($dto->password, $userData['password'])) {
            throw new \RuntimeException('Wrong password');
        }

        return new JsonResponse(['status' => 'ok', 'user_id' => $userData['id']]);
    }

    private function isUserExists(string $nickName): bool
    {
        return $this->storage->exists("{$this->storageDir}/{$nickName}.json");
    }
}
