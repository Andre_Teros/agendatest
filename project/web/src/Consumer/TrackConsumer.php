<?php
declare(strict_types=1);

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use SocialTech\StorageInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

class TrackConsumer implements ConsumerInterface
{
    private $decoder;
    private $storage;
    private $storageDir;

    public function __construct(
        DecoderInterface $decoder,
        StorageInterface $storage,
        string $storageDir
    ) {
        $this->decoder = $decoder;
        $this->storage = $storage;
        $this->storageDir = $storageDir;
    }

    public function execute(AMQPMessage $msg)
    {
        $data = $msg->getBody();

        $decodedData = $this->decoder->decode($data, 'json');
        $path = "{$this->storageDir}/{$decodedData['idUser']}.json";

        $this->storage->append($path, "{$data}\n");

        return true;
    }
}
