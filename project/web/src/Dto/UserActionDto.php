<?php
declare(strict_types=1);

namespace App\Dto;

class UserActionDto
{
    public $id;
    public $idUser;
    public $sourceLabel;
    public $dateCreated;

    public static function fromRequest(array $requestData): UserActionDto
    {
        $dto = new self();

        $dto->id = isset($requestData['id']) ? trim($requestData['id']) : null;
        $dto->idUser = isset($requestData['id_user']) ? trim($requestData['id_user']) : null;
        $dto->sourceLabel = isset($requestData['source_label']) ? trim($requestData['source_label']) : null;
        $dto->dateCreated = isset($requestData['date_created']) ? trim($requestData['date_created']) : null;

        return $dto;
    }
}
