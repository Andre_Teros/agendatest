<?php
declare(strict_types=1);

namespace App\Dto;

class UserAuthDto
{
    public $nickName;
    public $password;

    public static function fromRequest(array $requestData): UserAuthDto
    {
        $dto = new self();

        $dto->nickName = isset($requestData['nick_name']) ? trim($requestData['nick_name']) : null;
        $dto->password = isset($requestData['password']) ? trim($requestData['password']) : null;

        return $dto;
    }
}
