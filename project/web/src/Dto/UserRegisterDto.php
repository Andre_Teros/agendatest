<?php
declare(strict_types=1);

namespace App\Dto;

class UserRegisterDto
{
    public $id;
    public $firstName;
    public $lastName;
    public $nickName;
    public $password;
    public $age;

    public static function fromRequest(array $requestData): UserRegisterDto
    {
        $dto = new self();

        $dto->firstName = isset($requestData['first_name']) ? trim($requestData['first_name']) : null;
        $dto->lastName = isset($requestData['last_name']) ? trim($requestData['last_name']) : null;
        $dto->nickName = isset($requestData['nick_name']) ? trim($requestData['nick_name']) : null;
        $dto->password = isset($requestData['password']) ? trim($requestData['password']) : null;
        $dto->age = isset($requestData['age']) ? trim($requestData['age']) : null;

        return $dto;
    }
}
