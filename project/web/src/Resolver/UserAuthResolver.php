<?php
declare(strict_types=1);

namespace App\Resolver;

use App\Dto\UserAuthDto;
use App\Exceptions\WrongValidation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserAuthResolver implements ArgumentValueResolverInterface
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === UserAuthDto::class;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $data = $request->request->all();

        $dto = UserAuthDto::fromRequest($data);

        $this->validate($dto);

        yield $dto;
    }

    /**
     * @throws WrongValidation
     */
    private function validate($dto): void
    {
        $errors = $this->validator->validate($dto);

        if ($errors->count() !== 0) {
            throw new WrongValidation($errors);
        }
    }
}
