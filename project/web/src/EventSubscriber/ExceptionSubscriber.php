<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Exceptions\WrongValidation;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof WrongValidation) {
            /** @var WrongValidation $exception */
            $errorList = $exception->getErrorList();

            $output = [];
            foreach ($errorList as $error) {
                $output[] = [
                    'path' => $error->getPropertyPath(),
                    'message' => $error->getMessage(),
                ];
            }

            $response = new JsonResponse([
                'status' => 'validation error',
                'errors' => $output,
            ]);

            $event->setResponse($response);
        }

        if ($exception instanceof \RuntimeException) {
            /** @var WrongValidation $exception */
            $response = new JsonResponse([
                'status' => 'error',
                'message' => $exception->getMessage(),
            ]);

            $event->setResponse($response);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }
}
